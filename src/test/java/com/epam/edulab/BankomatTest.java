package com.epam.edulab;

import com.epam.edulab.util.ClientGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class BankomatTest {
    @Test
    public void test() throws InterruptedException {
        ClientGenerator clientGenerator = new ClientGenerator();
        Bankomat bankomat = new Bankomat();
        List<Client> clients = new ArrayList<>();
        ExecutorService threadPool = Executors.newCachedThreadPool();
        Random random = new Random();
        for (int i = 0; i < 100; i++) {
            clients.add(clientGenerator.generate());
        }
        long sumBefore = clients.stream().mapToLong(Client::getBalance).sum();
        for (int i = 0; i < 1000; i++) {
            int id = i;
            threadPool.execute(() -> {
                Client from = clients.get(random.nextInt(100));
                int clientId;
                while ((clientId = random.nextInt(100)) == from.getId()) {
                }
                Client to = clients.get(clientId);
                System.out.println("Execution number " + id);
                bankomat.transfer(from, to, random.nextInt(80000));
            });
        }
        threadPool.awaitTermination(25, TimeUnit.SECONDS);
        long sumAfter =  clients.stream().mapToLong(Client::getBalance).sum();
        assertEquals(sumBefore,sumAfter);
    }
}
