package com.epam.edulab;

import java.util.concurrent.locks.ReentrantLock;

public class Client {

    public int id;
    public String firstname;
    public String lastname;
    public volatile long balance;

    public Client(int id, String firstname, String lastname, int balance) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.balance = balance;
    }

    ReentrantLock reentrantLock = new ReentrantLock();

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }
    public int getId() {
        return id;
    }

    public void setId(int accounNumber) {
        this.id = id;
    }

    public ReentrantLock getReentrantLock() {
        return reentrantLock;
    }
}
