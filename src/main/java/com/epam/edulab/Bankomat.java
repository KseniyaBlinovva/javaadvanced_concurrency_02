package com.epam.edulab;

import com.epam.edulab.exeptions.IllegalAmountException;

public class Bankomat {
    public void transfer(Client from , Client to,int amount) {
        long balance = from.getBalance();
        if (balance < amount) throw new IllegalAmountException("Transfer amount " +amount+  " more than account balance "+from.getBalance());
        try {
            while (!(from.getReentrantLock().tryLock() && to.getReentrantLock().tryLock())) {

            }
            from.setBalance(balance - amount);
            to.setBalance(to.getBalance() + amount);
            System.out.println("Transfer " + amount + " from " + from.getId() + " to " + to.getId());
        }
        finally {
            from.getReentrantLock().unlock();
            to.getReentrantLock().unlock();
        }
    }
}
