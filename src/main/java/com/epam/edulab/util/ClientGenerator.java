package com.epam.edulab.util;

import com.epam.edulab.Client;

import java.util.Random;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClientGenerator {
    Random random = new Random();
    private String[] names = {"Arya","Tyrion","Cersei","Sansa","Varys","Bronn","Jon"};
    private String[] lastNames = {"Snow","Lannister","Stark","Clegane","Targaryen","Mormont","Greyjoy"};
    public Client generate() {
        String firstName = null;
        String lastName = null;
        int id = random.nextInt(10000);
        int balance = random.nextInt(100000);
        String firstname = names[random.nextInt(names.length)];
        String lastname = lastNames[random.nextInt(lastNames.length)];
        return new Client(id,firstname,lastname,balance);
    }
}