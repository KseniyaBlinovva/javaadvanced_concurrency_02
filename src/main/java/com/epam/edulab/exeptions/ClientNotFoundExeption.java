package com.epam.edulab.exeptions;

public class ClientNotFoundExeption extends RuntimeException {
    public ClientNotFoundExeption(String message) {
        super(message);
    }
}